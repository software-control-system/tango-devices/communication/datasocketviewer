static const char *RcsId = "$Header: /users/chaize/newsvn/cvsroot/Communication/DataSocketViewer/src/DataSocketViewerStateMachine.cpp,v 1.3 2011-07-15 09:28:35 buteau Exp $";
//+=============================================================================
//
// file :         DataSocketViewerStateMachine.cpp
//
// description :  C++ source for the DataSocketViewer and its alowed. 
//                method for commands and attributes
//
// project :      TANGO Device Server
//
// $Author: buteau $
//
// $Revision: 1.3 $
//
// $Log: not supported by cvs2svn $
// Revision 1.2  2009/07/30 14:31:36  buteau
// - MANTIS 0012203
//
//
// copyleft :     European Synchrotron Radiation Facility
//                BP 220, Grenoble 38043
//                FRANCE
//
//-=============================================================================
//
//  		This file is generated by POGO
//	(Program Obviously used to Generate tango Object)
//
//         (c) - Software Engineering Group - ESRF
//=============================================================================

#include <tango.h>
#include <DataSocketViewer.h>
#include <DataSocketViewerClass.h>

/*====================================================================
 *	This file contains the methods to allow commands and attributes
 * read or write execution.
 *
 * If you wand to add your own code, add it between 
 * the "End/Re-Start of Generated Code" comments.
 *
 * If you want, you can also add your own methods.
 *====================================================================
 */

namespace DataSocketViewer_ns
{

//=================================================
//		Attributes Allowed Methods
//=================================================


//=================================================
//		Commands Allowed Methods
//=================================================


}	// namespace DataSocketViewer_ns
